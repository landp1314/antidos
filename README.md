# Python antidos

> This project is not maintained actively by me. If you found something wrong (bugs, incorrect results) feel free to create merge request.

### Installation

```sh
git clone git clone https://landp1314@bitbucket.org/landp1314/antidos.git
cd antidos
python setup.py install
```

### Usage

```sh
usage: antidos [-h] (-i INPUT_INTERFACE | -f INPUT_FILE) [-c] [-u URL_MODEL] output

positional arguments:
  output                output file name (in flow mode) or directory (in sequence mode)

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT_INTERFACE    capture online data from INPUT_INTERFACE
  -f INPUT_FILE         capture offline data from INPUT_FILE
  -c, --csv, --flow     output flows as csv
```

Convert pcap file to flow csv:

```
antidos -f example.pcap -c flows.csv
```

Sniff packets real-time from interface to flow csv: (**need root permission**)

```
antidos -i eth0 -c flows.csv
```
